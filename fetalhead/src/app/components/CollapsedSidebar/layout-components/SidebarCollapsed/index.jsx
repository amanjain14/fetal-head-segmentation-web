import React from 'react';

import clsx from 'clsx';
import { Link } from 'react-router-dom';

import { UncontrolledTooltip } from 'reactstrap';

import PerfectScrollbar from 'react-perfect-scrollbar';
import { connect } from 'react-redux';
import { setSidebarToggleMobile } from '../../../../../reducers/ThemeOptions';

import { NavLink } from 'react-router-dom';

import projectLogo from '../../../../../assets/images/sickness.svg';
import { Monitor } from 'react-feather';

const SidebarCollapsed = (props) => {
  const toggleSidebarMobile = () => {
    setSidebarToggleMobile(!sidebarToggleMobile);
  };

  const {
    sidebarShadow,
    sidebarStyle,
    sidebarToggleMobile,
    setSidebarToggleMobile
  } = props;

  return (
    <>
      <div
        className={clsx(
          'app-sidebar app-sidebar--collapsed app-sidebar--mini',
          sidebarStyle,
          { 'app-sidebar--shadow': sidebarShadow }
        )}>
        <div className="app-sidebar--header">
          <div className="app-sidebar-logo">
            <Link
              to="/"
              title="Fetal Head Segmentation"
              className="app-sidebar-logo">
              <div className="app-sidebar-logo--icon">
                <img src={projectLogo} />
              </div>
            </Link>
          </div>
        </div>

        <div className="app-sidebar--content">
          <PerfectScrollbar>
            <ul className="mt-2 sidebar-menu-collapsed">
              <li>
                <NavLink
                  onClick={toggleSidebarMobile}
                  activeClassName="active"
                  to="/"
                  id="CollapsedSidebarTooltip123">
                  <span>
                    <Monitor />
                  </span>
                </NavLink>
                <UncontrolledTooltip
                  popperClassName="tooltip-secondary text-nowrap"
                  placement="right"
                  target="CollapsedSidebarTooltip123"
                  container=".app-sidebar--content"
                  boundariesElement="window">
                  Segmentation
                </UncontrolledTooltip>
              </li>
              {/* <li>
                <NavLink
                  onClick={toggleSidebarMobile}
                  activeClassName="active"
                  to="/"
                  id="CollapsedSidebarTooltip124">
                  <span>
                    <CloudDrizzle />
                  </span>
                </NavLink>
                <UncontrolledTooltip
                  popperClassName="tooltip-secondary text-nowrap"
                  placement="right"
                  target="CollapsedSidebarTooltip124"
                  container=".app-sidebar--content"
                  boundariesElement="window">
                  Calendar
                </UncontrolledTooltip>
              </li>
              <li>
                <NavLink
                  onClick={toggleSidebarMobile}
                  activeClassName="active"
                  to="/"
                  id="CollapsedSidebarTooltip125">
                  <span>
                    <Search />
                  </span>
                </NavLink>
                <UncontrolledTooltip
                  popperClassName="tooltip-secondary text-nowrap"
                  placement="right"
                  target="CollapsedSidebarTooltip125"
                  container=".app-sidebar--content"
                  boundariesElement="window">
                  File Manager
                </UncontrolledTooltip>
              </li> */}
            </ul>
          </PerfectScrollbar>
        </div>
      </div>
      <div
        onClick={toggleSidebarMobile}
        className={clsx('app-sidebar-overlay', {
          'is-active': sidebarToggleMobile
        })}
      />
    </>
  );
};

const mapStateToProps = (state) => ({
  sidebarShadow: state.ThemeOptions.sidebarShadow,
  sidebarStyle: state.ThemeOptions.sidebarStyle,
  sidebarToggleMobile: state.ThemeOptions.sidebarToggleMobile
});

const mapDispatchToProps = (dispatch) => ({
  setSidebarToggleMobile: (enable) => dispatch(setSidebarToggleMobile(enable))
});

export default connect(mapStateToProps, mapDispatchToProps)(SidebarCollapsed);
