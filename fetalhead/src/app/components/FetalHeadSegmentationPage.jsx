import React, { useState } from "react";
import CollapsedSidebar from "../components/CollapsedSidebar";
import WidgetsCarousels4 from "./example-components/WidgetsCarousels/Carousels4";
import WidgetsCarousels3 from "./example-components/WidgetsCarousels/Carousels3";
import { ClimbingBoxLoader } from "react-spinners";
function importAll(r) {
  return r.keys().map(r);
}
const images = importAll(require.context("../../assets/imagesFetal", false, /\.(png|jpe?g|svg)$/));

const SuspenseLoading = () => {
  return (
    <>
      <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
        <div className="d-flex align-items-center flex-column px-4">
          <ClimbingBoxLoader color={"#485563"} loading={true} />
        </div>
      </div>
    </>
  );
};

export default function WidgetsCarousels() {
  const [imageName, setImageName] = useState("");
  const [loading, setLoading] = useState(false);
  const [imageResultt, setImageResult] = useState("Testing.png");


  let imageSendName;

  const axios = require("axios");

  React.useEffect(() => {
    if (imageName.length > 1) {
      setLoading(true);
      imageSendName = imageName.split("/")[3].split(".");
      imageSendName = imageSendName[0] + "." + imageSendName[2];
      axios
        .post("/api/segmentation", {
          image: imageSendName
        })
        .then(function (response) {
          setImageResult(imageSendName);
          setLoading(false);
        });
    }
  }, [imageName]);

  return (
    <>
      <CollapsedSidebar>
        {loading ? <SuspenseLoading /> : <WidgetsCarousels3 imageResult={imageResultt}/>}
        <div className="ml-5 mr-5 mb-5">
          <WidgetsCarousels4 images={images} setImageName={setImageName} />
        </div>
      </CollapsedSidebar>
    </>
  );
}
