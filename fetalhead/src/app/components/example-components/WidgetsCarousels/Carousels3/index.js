import React from 'react';
import { Row, Col, Card } from 'reactstrap';
import Zoom from 'react-medium-image-zoom';
import 'react-medium-image-zoom/dist/styles.css';
import { ClimbingBoxLoader } from "react-spinners";
import Slider from 'react-slick';
const SuspenseLoading = () => {
  return (
    <>
      <div className="d-flex align-items-center flex-column vh-100 justify-content-center text-center py-3">
        <div className="d-flex align-items-center flex-column px-4">
          <ClimbingBoxLoader color={"#485563"} loading={true} />
        </div>
      </div>
    </>
  );
};
export default function LivePreviewExample(props) {
  const widgetsCarousels3A = {
    dots: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  return (
    <>
      <Row className="no-gutters pl-5 pb-5 pr-5 pt-3">
        <Col>
          <div className="bg-vicious-stance shadow-xxl rounded-lg p-5">
            <Col className="mx-auto">
              <Slider
                className="rounded-lg overflow-hidden slider-arrows-outside slider-dots-outside slider-dots-light"
                {...widgetsCarousels3A}>
                <div>
                  <Zoom>
                    <Card className="text-center m-3">
                      <Card className="card-transparent mx-auto mt-3">
                        <div className="card-img-wrapper rounded">
                          { props.imageResult ? <img
                            className="card-img-top rounded-sm"
                            src={process.env.PUBLIC_URL +"/" +props.imageResult }
                          />:<SuspenseLoading/>
                          }
                        </div>
                      </Card>
                    </Card>
                  </Zoom>
                </div>
              </Slider>
            </Col>
          </div>
        </Col>
      </Row>
    </>
  );
}
