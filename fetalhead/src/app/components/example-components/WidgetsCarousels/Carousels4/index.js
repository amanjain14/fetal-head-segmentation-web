import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Card } from "reactstrap";
import Slider from "react-slick";
import { LazyLoadImage } from "react-lazy-load-image-component";

function SliderArrowNext(props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FontAwesomeIcon icon={["fas", "chevron-right"]} />
    </div>
  );
}

function SliderArrowPrev(props) {
  const { className, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <FontAwesomeIcon icon={["fas", "chevron-left"]} />
    </div>
  );
}

export default function LivePreviewExample(props) {
  const widgetsCarousels4A = {
    // dots: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 3,
    arrows: true,
    nextArrow: <SliderArrowNext />,
    prevArrow: <SliderArrowPrev />,
    responsive: [
      {
        breakpoint: 1450,
        settings: { slidesToShow: 3, slidesToScroll: 1 }
      },
      {
        breakpoint: 1100,
        settings: { slidesToShow: 2, slidesToScroll: 1 }
      },
      {
        breakpoint: 900,
        settings: { slidesToShow: 1, slidesToScroll: 1 }
      }
    ]
  };
  return (
    <>
      <Card className="bg-vicious-stance py-4">
        <div lg={4}>
          <Slider
            className="rounded-lg overflow-hidden slider-arrows-outside slider-dots-outside slider-dots-light"
            {...widgetsCarousels4A}
          >
            {props.images.map((myList, x) => (
              <div key={x}>
                <a
                  href="#"
                  onClick={e => {
                    props.setImageName(myList);
                  }}
                  className="card m-3 shadow-sm-dark card-box-hover-rise"
                >
                  <LazyLoadImage src={myList} className="card-img-top rounded-bottom" alt="..." />
                </a>
              </div>
            ))}
          </Slider>
        </div>
      </Card>
    </>
  );
}
