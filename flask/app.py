from flask import Flask,request,jsonify
from flask_cors import CORS,cross_origin
import os
import cv2 as cv
import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.models import load_model
import matplotlib
from flask import Flask, request, send_file, Response, make_response
import matplotlib.pyplot as plt
from io import StringIO, BytesIO
import numpy as np
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import pandas as pd
import sys
import time
matplotlib.use("TKAgg")
print(matplotlib.get_backend())

from matplotlib import pyplot as plt

my_filetypes  = [("Image File",'.jpg'),("Image File",'.png'),("Image File",'.JPEG'),("Image File",'.BMP'), ("Image File",'.PNG')]
model = load_model('models/model_for_gui.h5')

background = [0,0,0]
fetus = [255, 0, 0]

label_colours = np.array([background, fetus])

def plot_images(predicted_mask):
    r = predicted_mask.copy()
    g = predicted_mask.copy()
    b = predicted_mask.copy()
    for l in range(0,2):
        r[predicted_mask==l]=label_colours[l,0]
        g[predicted_mask==l]=label_colours[l,1]
        b[predicted_mask==l]=label_colours[l,2]

    rgb = np.zeros((predicted_mask.shape[0], predicted_mask.shape[1], 3))
    rgb[:,:,0] = (r/255.0)
    rgb[:,:,1] = (g/255.0)
    rgb[:,:,2] = (b/255.0)
    return rgb

app=Flask(__name__,static_folder="../fetalhead/build",static_url_path='/')

#@app.route('/')
#def index():
#	return app.send_static_file('index.html')
CORS(app, support_credentials=True)
@app.route('/api/segmentation', methods=['POST', 'GET','OPTIONS'])
@cross_origin(supports_credentials=True)
def segmentation():
    if(request.method=='POST'):
        some_json=request.get_json()
        for key, value in some_json.items():
            k=value        
            cv_img = cv.imread(os.path.normpath(k))
            imgH=cv_img.shape[0]
            imgW=cv_img.shape[1]
            img = cv_img
            img = cv.resize(img, (224,224))
            e_img = np.expand_dims(img, axis=0)
            output = model.predict(e_img)
            pred =plot_images(np.argmax(output[0],axis=1).reshape((224,224)))
            pred = cv.resize(pred, (imgW,imgH))
            plt.figure(figsize = (10,5))
            plt.subplot(1,2,1)  
            plt.title("Original Image", fontsize=14)
            plt.imshow(cv_img)
            plt.subplot(1,2,2)
            plt.title("Fetus Detection", fontsize=14)
            plt.imshow(cv_img)
            plt.imshow(pred, 'jet', interpolation='none', alpha=0.3)
            plt.savefig("../fetalhead/build/"+k, format = 'png', dpi = 300)
            time.sleep(3)
            return jsonify({"key":some_json})
    else:
        return jsonify({"GET":"GET"})


if __name__=="__main__":
    app.run(host='0.0.0.0', port=5000)
